<?php
namespace org\bgdev\forums\database;

class MembersHelper extends AbstractHelper {
		
	public function getAllMemebers(int $offset, int $limit): array {
		$query =
				'select ' .
					//'* ' .
					'id, name, title, signature, mgroup, ' .
					'case when hide_email != 1 then email else null end as email, ' .
					'joined, avatar, posts, ' .
					'last_post, last_visit, last_activity, ' .
					'cast(substring_index(view_prefs, \'&\', 1) as signed) as posts_per_page, ' .
					'cast(substring_index(view_prefs, \'&\', -1) as signed) as topics_per_page ' .
				'from ibf_members ' .
				'where id > 0 ' . // skip Guest
				'order by name asc ' .
				'limit :offset, :limit';
		
		return $this->getDBHelper()->fetchAll($query, [
			'offset' => $offset,
			'limit' => $limit
		]);
	}
	
	public function getMemeber(int $member_id): array {
		$query =
				'select ' .
					//'* ' .
					'id, name, title, signature, mgroup, ' .
					'case when hide_email != 1 then email else null end as email, ' .
					'joined, avatar, posts, ' .
					'last_post, last_visit, last_activity, ' .
					'cast(substring_index(view_prefs, \'&\', 1) as signed) as posts_per_page, ' .
					'cast(substring_index(view_prefs, \'&\', -1) as signed) as topics_per_page ' .
				'from ibf_members ' .
				'where id = :member_id';
		
		return $this->getDBHelper()->fetch($query, [
			'member_id' => $member_id
		]);
	}
	
	public function getMembersOnLine(): array {
		$query =
				'select ' .
					'case when login_type != 1 then member_id else 0 end as member_id, ' .
					'case when login_type != 1 then member_name else \'Annon\' end as member_name, ' .
					'count(*) as members_count ' .
				'from ibf_sessions ' .
				'where unix_timestamp() - running_time < :last_activity ' .
				'group by member_id, member_name ' .
				'order by running_time desc';
		
		return $this->getDBHelper()->fetchAll($query, [
			'last_activity' => LAST_ACTIVITY
		]);
	}
	
	public function getMembersInCategory(int $member_id, int $category_id): array {
		$query =
				'select ' .
					'case when login_type != 1 then member_id else 0 end as member_id, ' .
					'case when login_type != 1 then member_name else \'Annon\' end as member_name, ' .
					'count(*) as members_count ' .
				'from ibf_sessions ' .
				'where in_forum in (' .
					'select id from ibf_forums where category = :category_id' .
				') or member_id = :member_id and ' .
					'unix_timestamp() - running_time < :last_activity ' .
				'group by member_id, member_name ' .
				'order by running_time desc';
		
		return $this->getDBHelper()->fetchAll($query, [
			'member_id' => $member_id,
			'category_id' => $category_id,
			'last_activity' => LAST_ACTIVITY
		]);
	}
	
	public function getMembersInForum(int $member_id, int $forum_id): array {
		$query =
				'select ' .
					'case when login_type != 1 then member_id else 0 end as member_id, ' .
					'case when login_type != 1 then member_name else \'Annon\' end as member_name, ' .
					'count(*) as members_count ' .
				'from ibf_sessions ' .
				'where member_id = :member_id or in_forum = :forum_id and ' .
					'unix_timestamp() - running_time < :last_activity ' .
				'group by member_id, member_name ' .
				'order by running_time desc';
		
		return $this->getDBHelper()->fetchAll($query, [
			'member_id' => $member_id,
			'forum_id' => $forum_id,
			'last_activity' => LAST_ACTIVITY
		]);
	}
	
	public function getMembersInTopic(int $member_id, int $topic_id): array {
		$query =
				'select ' .
					'case when login_type != 1 then member_id else 0 end as member_id, ' .
					'case when login_type != 1 then member_name else \'Annon\' end as member_name, ' .
					'count(*) as members_count ' .
				'from ibf_sessions ' .
				'where member_id = :member_id or in_topic = :topic_id and ' .
					'unix_timestamp() - running_time < :last_activity ' .
				'group by member_id, member_name ' .
				'order by running_time desc';
		
		return $this->getDBHelper()->fetchAll($query, [
			'member_id' => $member_id,
			'topic_id' => $topic_id,
			'last_activity' => LAST_ACTIVITY
		]);
	}
}
