<?php
namespace org\bgdev\forums\database;

class MessagesHelper extends AbstractHelper {
	const INBOX = 'in';
	const OUTBOX = 'sent';
	
	public function getMessage(int $message_id): array {
		$query = 
				'select ' .
					'msg_id as id, msg_date, vid, ' .
					'from_id, (select name from ibf_members where id = from_id) as name, read_date, ' .
					'title, message ' .
				'from ibf_messages ' .
				'where msg_id = :msg_id and member_id = :member_id';
		
		return $this->getDBHelper()->fetch($query, [
			'msg_id' => $message_id,
			'member_id' => $this->getSession()->getMemberId()
		]);
	}

	private function getAllMessages(string $type, int $offset, int $limit): array {		
		$query = 
				'select ' .
					'msg_id as id, msg_date, ' .
					'from_id, (select name from ibf_members where id = from_id) as name, read_date, ' .
					'title, message ' .
				'from ibf_messages ' .
				'where member_id = :member_id and vid = :type ' .
				'order by msg_date desc ' . 
				'limit :offset, :limit';
		
		return $this->getDBHelper()->fetchAll($query, [
			'type' => $type,
			'member_id' => $this->getSession()->getMemberId(),
			'offset' => $offset,
			'limit' => $limit
		]);
	}
	
	public function getInbox(int $offset, int $limit): array {
		return $this->getAllMessages(MessagesHelper::INBOX, $offset, $limit);
	}

	public function getOutbox(int $offset, int $limit): array {
		return $this->getAllMessages(MessagesHelper::OUTBOX, $offset, $limit);
	}
}
